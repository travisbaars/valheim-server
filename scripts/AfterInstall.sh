#!/bin/bash

if [ $(dpkg-query -W -f='${Status}' docker-ce 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    groupadd docker
    usermod -aG docker ubuntu
    newgrp docker

    systemctl enable docker.service
    systemctl enable containerd.service
fi
