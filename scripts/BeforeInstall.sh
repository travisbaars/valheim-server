#!/bin/bash

apt update

if [ $(dpkg-query -W -f='${Status}' wget 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    apt install wget
fi

if [ $(dpkg-query -W -f='${Status}' curl 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    apt install curl
fi

if [ $(dpkg-query -W -f='${Status}' docker-ce 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    curl -fsSL https://get.docker.com -o get-docker.sh
    sh get-docker.sh
fi

if [ $(dpkg-query -W -f='${Status}' tmux 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    apt install tmux
fi

wget https://raw.githubusercontent.com/travisbaars/dotfiles/main/.tmux.conf

sudo apt upgrade -y